package com.example.location;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.map.MapController;
import com.baidu.platform.comapi.basestruct.GeoPoint;

public class MyActivity extends Activity {

    public final static String EXTRA_MESSAGE = "com.example.MESSAGE";

    BMapManager mBMapMan = null;
    MyMapView mMapView = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mBMapMan = new BMapManager(getApplication());
        mBMapMan.init("F5c0ee031e396bc1d8f044ec86103067", null);
        //注意：请在试用setContentView前初始化BMapManager对象，否则会报错
        setContentView(R.layout.main);
        mMapView = (MyMapView) findViewById(R.id.bmapsView);
        mMapView.setBuiltInZoomControls(true);
        //设置启用内置的缩放控件
        MapController mMapController = mMapView.getController();
        // 得到mMapView的控制权,可以用它控制和驱动平移和缩放
        GeoPoint point = new GeoPoint((int) (22.355 * 1E6), (int) (113.593 * 1E6));
        //用给定的经纬度构造一个GeoPoint，单位是微度 (度 * 1E6)
        mMapController.setCenter(point);//设置地图中心点
        mMapController.setZoom(12);//设置地图zoom级别
    }


    @Override
    protected void onDestroy() {
        mMapView.destroy();
        if (mBMapMan != null) {
            mBMapMan.destroy();
            mBMapMan = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        if (mBMapMan != null) {
            mBMapMan.stop();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        if (mBMapMan != null) {
            mBMapMan.start();
        }
        super.onResume();
    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, add.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}

