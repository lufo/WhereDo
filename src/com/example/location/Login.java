package com.example.location;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created with IntelliJ IDEA.
 * User: lufo816
 * Date: 13-11-8
 * Time: 下午10:11
 * To change this template use File | Settings | File Templates.
 */
public class Login extends Activity {
    public final static String EXTRA_MESSAGE = "com.example.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Intent intent = getIntent();
    }

    public void login(View view) {
        Intent intent = new Intent(this, FirstAdd.class);
        String message = "亲，待办事项已全部完成！";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void register(View view) {
        Intent intent = new Intent(this, register.class);
        startActivity(intent);
    }

}
