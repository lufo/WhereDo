package com.example.location;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.PopupOverlay;
import com.baidu.platform.comapi.basestruct.GeoPoint;

/**
 * Created with IntelliJ IDEA.
 * User: lufo816
 * Date: 13-10-27
 * Time: 下午1:36
 * To change this template use File | Settings | File Templates.
 */

class MyMapView extends MapView {
    static PopupOverlay pop = null;//弹出泡泡图层，点击图标使用

    public MyMapView(Context context) {
        super(context);
    }

    public MyMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyMapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //获得屏幕点击的位置
        int x = (int) event.getX();
        int y = (int) event.getY();
        //将像素坐标转为地址坐标
        GeoPoint pt = this.getProjection().fromPixels(x, y);
        int latitude = pt.getLatitudeE6();
        int longitude = pt.getLongitudeE6();
        return super.onTouchEvent(event);
    }
}
