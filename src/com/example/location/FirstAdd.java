package com.example.location;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: lufo816
 * Date: 13-11-6
 * Time: 上午10:03
 * To change this template use File | Settings | File Templates.
 */
public class FirstAdd extends Activity {
    public ListView listView;
    LinearLayout mainline;
    View itemview;
    Button button;
    String message;
    List<String> data = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add);
        mainline = (LinearLayout) findViewById(R.id.testline1);
        // Get the message from the intent
        Intent intent = getIntent();
        message = intent.getStringExtra(Login.EXTRA_MESSAGE);
        itemview = View.inflate(this, R.layout.testitem, null);
        itemview.setId(1);
        mainline.addView(itemview);
        button = (Button) itemview.findViewById(R.id.button_add);
        button.setText("添加");
        listView = (ListView) itemview.findViewById(R.id.listview);
        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, getData()));
    }

    public List<String> getData() {
        data.add(message);
        return data;
    }

    public void add(View view) {
        Intent intent = new Intent(this, MyActivity.class);
        startActivity(intent);
    }
}

